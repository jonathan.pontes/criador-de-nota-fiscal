package com.notafiscal.producer;

import com.notafiscal.model.Logger;
import com.notafiscal.model.NfeCreated;
import com.notafiscal.model.TipoLog;
import com.notafiscal.security.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class NfeCreatedProducer {

    @Autowired
    private KafkaTemplate<String, NfeCreated> producerNfeCreated;

    @Autowired
    private KafkaTemplate<String, Logger> producerLogger;


    public NfeCreated cadastrarNfe(NfeCreated nfeCreated) {

        // ENVIAR PARA O KAFKA
        producerNfeCreated.send("spec2-jonathan-roberto-1", nfeCreated);

        System.out.println("Nova Nota postada na fila: "
                + "\nCNPJ/CPF: " + nfeCreated.getIdentidade()
                + "\nValor: " + nfeCreated.getValor()
                + "\nStatus: " + nfeCreated.getStatus());
        return nfeCreated;
    }

    public void enviarLog(Usuario usuario, TipoLog tipoLog, NfeCreated nfeCreated) {
        Logger logger = new Logger();
        logger.setUsuarioName(usuario.getName());
        logger.setDataHora(LocalDateTime.now().toString());
        logger.setIdentidade(nfeCreated.getIdentidade());
        logger.setTipoLog(tipoLog);
        logger.setValor(nfeCreated.getValor().toString());

        // ENVIAR PARA O KAFKA
        producerLogger.send("spec2-jonathan-roberto-3", logger);

        System.out.println("Log enviado : "
                + "\nId: " + logger.getUsuarioName()
                + "\nData: " + logger.getDataHora()
                + "\nIdentidade: " + logger.getIdentidade()
                + "\nTipo: " + logger.getTipoLog().getDescricao());

    }

}
