package com.notafiscal.controller;

import com.notafiscal.model.dto.NfeCreateRequest;
import com.notafiscal.model.dto.NfeCreateResponse;
import com.notafiscal.model.dto.NfeSearchResponse;
import com.notafiscal.model.dto.NfeUpdateRequest;
import com.notafiscal.model.mapper.NfeCreatedMapper;
import com.notafiscal.security.Usuario;
import com.notafiscal.service.NfeCreatedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/nfe")
public class NfeCreatedController {

    @Autowired
    NfeCreatedService service;

    @PostMapping("/emitir")
    @ResponseStatus(code = HttpStatus.CREATED)
    public NfeCreateResponse emitir(@RequestBody NfeCreateRequest request, @AuthenticationPrincipal Usuario usuario) {
        return NfeCreatedMapper.toCreateResponse(service.cadastrar(NfeCreatedMapper.fromCreateRequest(request), usuario));
    }

    @PutMapping("/atualizar/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void atualizar(@RequestBody NfeUpdateRequest request, @PathVariable Long id) {
        request.setId(id);
        service.atualizar(NfeCreatedMapper.fromUpdateRequest(request));
    }

    @GetMapping("/consultar/{identidade}")
    @ResponseStatus(HttpStatus.OK)
    public List<NfeSearchResponse> buscarPorIdentidade(@PathVariable String identidade, @AuthenticationPrincipal Usuario usuario) {
        return NfeCreatedMapper.toSearchResponse(service.buscarPorIdentidade(identidade, usuario));
    }
}
