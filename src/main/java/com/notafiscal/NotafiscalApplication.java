package com.notafiscal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class NotafiscalApplication {

	public static void main(String[] args) {
		SpringApplication.run(NotafiscalApplication.class, args);
	}

}
