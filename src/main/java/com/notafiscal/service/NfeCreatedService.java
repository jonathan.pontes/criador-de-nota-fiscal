package com.notafiscal.service;

import com.notafiscal.model.NfeCreated;
import com.notafiscal.model.TipoLog;
import com.notafiscal.producer.NfeCreatedProducer;
import com.notafiscal.repository.NfeCreatedRepository;
import com.notafiscal.security.Usuario;
import com.notafiscal.utils.exceptions.NfeCreatedNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
public class NfeCreatedService {

    @Autowired
    private NfeCreatedRepository repository;

    @Autowired
    private NfeCreatedProducer producer;

    public NfeCreated cadastrar(NfeCreated nfeCreated, Usuario usuario) {
        nfeCreated.setIdentidade(removerformacao(nfeCreated.getIdentidade()));

        //SALVA NO BANCO DE DADOS
        NfeCreated nfeCreatedCadastrada = repository.save(nfeCreated);

        //LOGAR ACESSOS
        producer.enviarLog(usuario, TipoLog.EMISSAO, nfeCreated);

        //ENVIAR PARA O KAFKA
        return producer.cadastrarNfe(nfeCreatedCadastrada);
    }

    public void atualizar(NfeCreated nfeCreated) {
        Optional<NfeCreated> optional = repository.findById(nfeCreated.getId());

        if (optional.isPresent()) {
            nfeCreated.setValor(optional.get().getValor());
            nfeCreated.setIdentidade(optional.get().getIdentidade());

            repository.save(nfeCreated);
        } else {
            throw new NfeCreatedNotFoundException();
        }
    }

    public List<NfeCreated> buscarPorIdentidade(String identidade, Usuario usuario) {
        NfeCreated nfeCreated = new NfeCreated();
        nfeCreated.setIdentidade(identidade);
        nfeCreated.setValor(new BigDecimal("0.0"));
        List<NfeCreated> lista = repository.findByIdentidade(identidade);

        producer.enviarLog(usuario, TipoLog.CONSULTA, nfeCreated);

        return lista;
    }

    private String removerformacao(String identidade) {
        return identidade.trim().replaceAll("[./-]", "");
    }

}
