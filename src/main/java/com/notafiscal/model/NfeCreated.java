package com.notafiscal.model;


import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class NfeCreated {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String identidade;
    private BigDecimal valor;
    private String status;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "imposto_id", referencedColumnName = "id")
    private Imposto imposto;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Imposto getImposto() {
        return imposto;
    }

    public void setImposto(Imposto imposto) {
        this.imposto = imposto;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIdentidade() {
        return identidade;
    }

    public void setIdentidade(String identidade) {
        this.identidade = identidade;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }
}
