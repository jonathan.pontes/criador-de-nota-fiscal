package com.notafiscal.model.mapper;

import com.notafiscal.model.NfeCreated;
import com.notafiscal.model.dto.NfeCreateRequest;
import com.notafiscal.model.dto.NfeCreateResponse;
import com.notafiscal.model.dto.NfeSearchResponse;
import com.notafiscal.model.dto.NfeUpdateRequest;

import java.util.ArrayList;
import java.util.List;

public class NfeCreatedMapper {

    public static NfeCreated fromCreateRequest(NfeCreateRequest request) {
        NfeCreated nfeCreated = new NfeCreated();
        nfeCreated.setIdentidade(request.getIdentidade());
        nfeCreated.setValor(request.getValor());
        nfeCreated.setStatus("pending");
        nfeCreated.setImposto(null);

        return nfeCreated;
    }

    public static NfeCreateResponse toCreateResponse(NfeCreated nfeCreated) {
        NfeCreateResponse response = new NfeCreateResponse();
        response.setStatus(nfeCreated.getStatus());

        return response;
    }

    public static NfeCreated fromUpdateRequest(NfeUpdateRequest request) {
        NfeCreated nfeCreated = new NfeCreated();
        nfeCreated.setId(request.getId());
        nfeCreated.setImposto(request.getImposto());
        nfeCreated.setStatus(request.getStatus());
        return nfeCreated;
    }

    public static List<NfeSearchResponse> toSearchResponse(List<NfeCreated> nfes) {
        List<NfeSearchResponse> responses = new ArrayList<>();

        for (NfeCreated nfeCreated : nfes) {
            NfeSearchResponse response = new NfeSearchResponse();

            response.setIdentidade(nfeCreated.getIdentidade());
            response.setValor(nfeCreated.getValor());
            response.setStatus(nfeCreated.getStatus());
            response.setImposto(nfeCreated.getImposto());

            responses.add(response);
        }
        return responses;
    }
}
