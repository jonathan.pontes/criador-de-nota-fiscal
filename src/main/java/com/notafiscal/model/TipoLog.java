package com.notafiscal.model;

public enum TipoLog {

    EMISSAO("Emissão"),
    CONSULTA("Consulta");

    private String descricao;

    TipoLog(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
}
