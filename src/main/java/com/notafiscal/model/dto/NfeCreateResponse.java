package com.notafiscal.model.dto;

public class NfeCreateResponse {
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
