package com.notafiscal.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.notafiscal.model.Imposto;

import java.math.BigDecimal;

public class NfeSearchResponse {

    private String identidade;
    private BigDecimal valor;
    private String status;

    @JsonProperty("nfe")
    private Imposto imposto;

    public String getIdentidade() {
        return identidade;
    }

    public void setIdentidade(String identidade) {
        this.identidade = identidade;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Imposto getImposto() {
        return imposto;
    }

    public void setImposto(Imposto imposto) {
        this.imposto = imposto;
    }
}
