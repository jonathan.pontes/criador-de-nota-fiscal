package com.notafiscal.model.dto;

import java.math.BigDecimal;

public class NfeCreateRequest {

    private String identidade;
    private BigDecimal valor;

    public String getIdentidade() {
        return identidade;
    }

    public void setIdentidade(String identidade) {
        this.identidade = identidade;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }
}
