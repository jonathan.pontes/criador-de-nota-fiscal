package com.notafiscal.utils.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Nota fiscal não encontrada!")
public class NfeCreatedNotFoundException extends RuntimeException {
}
