package com.notafiscal.repository;

import com.notafiscal.model.NfeCreated;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface NfeCreatedRepository extends CrudRepository<NfeCreated, Long> {

    List<NfeCreated> findByIdentidade(String identidade);
}
